<?php
/*
Template Name: contact-page
*/
?>
<?php get_header(); ?>
        <div id="gdlr-header-substitute" style="height: auto;"></div>
        <div class="gdlr-nav-search-form" id="gdlr-nav-search-form" style="">
            <div class="gdlr-nav-search-container container">
                <form method="get" action="http://demo.goodlayers.com/clevercourse">
                    <i class="fa fa-search icon-search"></i>
                    <input type="submit" id="searchsubmit" class="style-2" value="">
                    <div class="search-text" id="search-text">
                        <input type="text" value="" name="s" id="s" autocomplete="off" data-default="Type keywords...">
                    </div>
                    <div class="clear"></div>
                </form>
            </div>
        </div>


        <!-- is search -->
        <div class="content-wrapper">
            <div class="gdlr-content">

                <!-- Above Sidebar Section-->
                <div class="above-sidebar-wrapper">
                    <section id="content-section-1">
                        <div class="gdlr-full-size-wrapper gdlr-show-all"
                             style="padding-bottom: 0px;  background-color: #ffffff; ">
                            <div class="gdlr-item gdlr-content-item" style="margin-bottom: 0px;">


                            <?php dynamic_sidebar( 'contact_page_above_sidebar' ); ?>

                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </section>
                </div>


                <!-- Sidebar With Content Section-->
                <div class="with-sidebar-wrapper gdlr-type-right-sidebar">
                    <div class="with-sidebar-container container">
                        <div class="with-sidebar-left eight columns">
                            <div class="with-sidebar-content twelve columns">
                                <section id="content-section-2">
                                    <div class="section-container container">
                                        <div class="gdlr-item gdlr-content-item" style="margin-bottom: 60px;"><p></p>
                                            <div class="clear"></div>

                                            <!-- Content sidebar -->
	                                        <?php dynamic_sidebar( 'contact_page_content_sidebar' ); ?>

                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>

                            <div class="clear"></div>
                        </div>

                        <div class="gdlr-sidebar gdlr-right-sidebar four columns">
                            <div class="gdlr-item-start-content sidebar-right-item">

                                <!-- Right sidebar -->
	                            <?php dynamic_sidebar( 'contact_page_right_sidebar' ); ?>

                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>


                <!-- Below Sidebar Section-->
                <div class="below-sidebar-wrapper">
                    <section id="content-section-3">
                        <div class="gdlr-parallax-wrapper gdlr-background-image gdlr-show-all no-skin"
                             id="gdlr-parallax-wrapper-1" data-bgspeed="0.2"
                             style="background-image: url(&quot;http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/uploads/2013/11/photodune-4503767-skyline-l.jpg&quot;); padding-top: 100px; padding-bottom: 50px; background-position: center -0.6px;">

                            <div class="container">

                                <?php dynamic_sidebar( 'contact_page_below_sidebar' ); ?>

                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </section>
                </div>


            </div><!-- gdlr-content -->
            <div class="clear"></div>
        </div><!-- content wrapper -->


    <script>
        (function (body) {
            'use strict';
            body.className = body.className.replace(/\btribe-no-js\b/, 'tribe-js');
        })(document.body);
    </script>
    <script type="text/javascript"></script>
    <script type="text/javascript"> /* <![CDATA[ */
        var tribe_l10n_datatables = {
            "aria": {
                "sort_ascending": ": activate to sort column ascending",
                "sort_descending": ": activate to sort column descending"
            },
            "length_menu": "Show _MENU_ entries",
            "empty_table": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "info_empty": "Showing 0 to 0 of 0 entries",
            "info_filtered": "(filtered from _MAX_ total entries)",
            "zero_records": "No matching records found",
            "search": "Search:",
            "pagination": {"all": "All", "next": "Next", "previous": "Previous"},
            "select": {"rows": {"0": "", "_": ": Selected %d rows", "1": ": Selected 1 row"}},
            "datepicker": {
                "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
                "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                "monthNamesShort": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                "nextText": "Next",
                "prevText": "Prev",
                "currentText": "Today",
                "closeText": "Done"
            }
        };
        /* ]]> */ </script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var _wpcf7 = {"recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}, "cached": "1"};
        /* ]]> */
    </script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.7"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4"></script>
    <script type="text/javascript">
        jQuery(document).ready(function (jQuery) {
            jQuery.datepicker.setDefaults({
                "closeText": "Close",
                "currentText": "Today",
                "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                "nextText": "Next",
                "prevText": "Previous",
                "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
                "dateFormat": "MM d, yy",
                "firstDay": 1,
                "isRTL": false
            });
        });
    </script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/goodlayers-lms/lms-script.js?ver=1.0.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/superfish/js/superfish.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-includes/js/hoverIntent.min.js?ver=1.8.1"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/dl-menu/modernizr.custom.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/dl-menu/jquery.dlmenu.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/jquery.easing.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/fancybox/jquery.fancybox.pack.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/fancybox/helpers/jquery.fancybox-media.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/fancybox/helpers/jquery.fancybox-thumbs.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/flexslider/jquery.flexslider.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/javascript/gdlr-script.js?ver=1.0"></script>
    <script type="text/javascript"
            src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-includes/js/wp-embed.min.js?ver=721dbbaadbdc2a4c219f964ae2366d30"></script>
    <script type="text/css" id="tmpl-tribe_customizer_css">.tribe-events-list .tribe-events-loop .tribe-event-featured,
        .tribe-events-list #tribe-events-day.tribe-events-loop .tribe-event-featured,
        .type-tribe_events.tribe-events-photo-event.tribe-event-featured .tribe-events-photo-event-wrap,
        .type-tribe_events.tribe-events-photo-event.tribe-event-featured .tribe-events-photo-event-wrap:hover {
            background-color: #0ea0d7;
        }

        #tribe-events-content table.tribe-events-calendar .type-tribe_events.tribe-event-featured {
            background-color: #0ea0d7;
        }

        .tribe-events-list-widget .tribe-event-featured,
        .tribe-events-venue-widget .tribe-event-featured,
        .tribe-mini-calendar-list-wrapper .tribe-event-featured,
        .tribe-events-adv-list-widget .tribe-event-featured .tribe-mini-calendar-event {
            background-color: #0ea0d7;
        }

        .tribe-grid-body .tribe-event-featured.tribe-events-week-hourly-single {
            background-color: rgba(14, 160, 215, .7);
            border-color: #0ea0d7;
        }

        .tribe-grid-body .tribe-event-featured.tribe-events-week-hourly-single:hover {
            background-color: #0ea0d7;
        }</script>
    <style type="text/css" id="tribe_customizer_css">.tribe-events-list .tribe-events-loop .tribe-event-featured,
        .tribe-events-list #tribe-events-day.tribe-events-loop .tribe-event-featured,
        .type-tribe_events.tribe-events-photo-event.tribe-event-featured .tribe-events-photo-event-wrap,
        .type-tribe_events.tribe-events-photo-event.tribe-event-featured .tribe-events-photo-event-wrap:hover {
            background-color: #0ea0d7;
        }

        #tribe-events-content table.tribe-events-calendar .type-tribe_events.tribe-event-featured {
            background-color: #0ea0d7;
        }

        .tribe-events-list-widget .tribe-event-featured,
        .tribe-events-venue-widget .tribe-event-featured,
        .tribe-mini-calendar-list-wrapper .tribe-event-featured,
        .tribe-events-adv-list-widget .tribe-event-featured .tribe-mini-calendar-event {
            background-color: #0ea0d7;
        }

        .tribe-grid-body .tribe-event-featured.tribe-events-week-hourly-single {
            background-color: rgba(14, 160, 215, .7);
            border-color: #0ea0d7;
        }

        .tribe-grid-body .tribe-event-featured.tribe-events-week-hourly-single:hover {
            background-color: #0ea0d7;
        }</style>


<?php get_footer(); ?>