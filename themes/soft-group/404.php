<?php get_header(); ?>
<?php //get_template_part( 'loop' ); ?>

    <div class="page-not-found-container container">
        <div class="gdlr-item page-not-found-item">
            <div class="page-not-found-block">
                <div class="page-not-found-icon">
                    <i class="fa fa-frown-o icon-frown"></i>
                </div>
                <div class="page-not-found-title">
                    Error 404
                </div>
                <div class="page-not-found-caption">
                    Sorry, we couldn't find the page you're looking for.
                </div>
                <div class="page-not-found-search">
                    <div class="gdl-search-form">
	                    <?php the_widget( 'WP_Widget_Search' ); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>