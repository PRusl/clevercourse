<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain( 'sg', get_template_directory() . '/languages' );


/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags() {
	?>
    <link rel="shortcut icon" href="<?php bloginfo( "template_url" ); ?>/favico.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- links from clevercourse -->
    <link rel="pingback" href="http://demo.goodlayers.com/clevercourse/xmlrpc.php"/>
    <link rel='dns-prefetch' href='//maps.google.com'/>
    <link rel='dns-prefetch' href='//ajax.googleapis.com'/>
    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> &raquo; Feed"
          href="http://demo.goodlayers.com/clevercourse/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> &raquo; Comments Feed"
          href="http://demo.goodlayers.com/clevercourse/comments/feed/"/>
    <link rel="alternate" type="text/calendar" title="<?php bloginfo( 'name' ); ?> &raquo; iCal Feed"
          href="http://demo.goodlayers.com/clevercourse/events/?ical=1"/>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> &raquo; Homepage Comments Feed"
          href="http://demo.goodlayers.com/clevercourse/homepage/feed/"/>
    <link rel='stylesheet' id='contact-form-7-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.7'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='font-awesome-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/goodlayers-lms/font-awesome-new/css/font-awesome.min.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='gdlr-date-picker-css'
          href='//ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='lms-style-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/goodlayers-lms/lms-style.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='lms-style-custom-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/goodlayers-lms/lms-style-custom.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/style.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='Raleway-google-font-css'
          href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2Cregular%2C500%2C600%2C700%2C800%2C900&#038;subset=latin&#038;ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='Montserrat-google-font-css'
          href='http://fonts.googleapis.com/css?family=Montserrat%3Aregular%2C700&#038;subset=latin&#038;ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='superfish-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/superfish/css/superfish.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='dlmenu-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/dl-menu/component.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <!--[if lt IE 8]>
    <link rel='stylesheet' id='font-awesome-ie7-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/font-awesome/css/font-awesome-ie7.min.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <![endif]-->
    <link rel='stylesheet' id='jquery-fancybox-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/fancybox/jquery.fancybox.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='flexslider-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/plugins/flexslider/flexslider.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='style-responsive-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/stylesheet/style-responsive.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='style-custom-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/stylesheet/style-custom.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='wpgmp-frontend-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/wp-google-map-plugin//assets/css/frontend.css?ver=721dbbaadbdc2a4c219f964ae2366d30'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='ms-main-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/plugins/masterslider/public/assets/css/masterslider.main.css?ver=3.1.3'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='ms-custom-css'
          href='http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/uploads/masterslider/custom.css?ver=2.6'
          type='text/css' media='all'/>
    <link rel='https://api.w.org/' href='http://demo.goodlayers.com/clevercourse/wp-json/'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="https://demo.goodlayers.com/clevercourse/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-includes/wlwmanifest.xml"/>

    <link rel="canonical" href="http://demo.goodlayers.com/clevercourse/"/>
    <link rel='shortlink' href='http://demo.goodlayers.com/clevercourse/'/>
    <link rel="alternate" type="application/json+oembed"
          href="http://demo.goodlayers.com/clevercourse/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdemo.goodlayers.com%2Fclevercourse%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="http://demo.goodlayers.com/clevercourse/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdemo.goodlayers.com%2Fclevercourse%2F&#038;format=xml"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php
}

add_action( 'wp_head', 'add_head_meta_tags' );


/**
 * Реєструємо місце для меню
 */

register_nav_menus( array(
	'primary' => __( 'Primary', 'sg' ),
) );


/**
 * Реєструємо сайдбари теми
 */

if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'top_left_sidebar',
		'name'          => __( 'Top left sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div class="top-navigation-left">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => ''
	) );
}
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'top_right_sidebar',
		'name'          => __( 'Top right sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div class="top-social-wrapper">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => ''
	) );
}
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'left_sidebar',
		'name'          => __( 'Left sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>'
	) );
}
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'right_sidebar',
		'name'          => __( 'Right sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div id="text" class="widget widget_text gdlr-item gdlr-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="gdlr-widget-title">',
		'after_title'   => '</h3><div class="clear"></div>'
	) );
}
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'content_sidebar',
		'name'          => __( 'Content sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div class="with-sidebar-wrapper">',
		'after_widget'  => '</section></div>',
		'before_title'  => '<section id="content-section">',
		'after_title'   => ''
	) );
}

if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'footer_sidebar',
		'name'          => __( 'Footer sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div class="footer-column three columns" id="footer-widget-1">',
		'after_widget'  => '</div>',
		'before_title'  => '<div id="text-5" class="widget widget_text gdlr-item gdlr-widget">',
		'after_title'   => '</div>'
	) );
}
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'copyright_sidebar',
		'name'          => __( 'Copyright sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div class="copyright-container container">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => ''
	) );
}

/* Sidebars for contact-page */
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'contact_page_above_sidebar',
		'name'          => __( 'Contact page above sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div class="gdlr-item gdlr-content-item" style="margin-bottom: 0px;">',
		'after_widget'  => '</div><div class="clear"></div>',
		'before_title'  => '<h3 class="gdlr-widget-title">',
		'after_title'   => '</h3><div class="clear"></div>'
	) );
}
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'contact_page_content_sidebar',
		'name'          => __( 'Contact page content sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<section id="content-section-2">',
		'after_widget'  => '</section>',
		'before_title'  => ' <div class="gdlr-space" style="margin-top: -22px;"></div>
                                            <h5 class="gdlr-heading-shortcode " style="font-weight: bold;">',
		'after_title'   => '</h5><div class="clear"></div>'
	) );
}
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'contact_page_right_sidebar',
		'name'          => __( 'Contact page right sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div id="text" class="widget widget_text gdlr-item gdlr-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="gdlr-widget-title">',
		'after_title'   => '</h3><div class="clear"></div>'
	) );
}
if ( function_exists( 'register_sidebar' ) ) {
	register_sidebars( 1, array(
		'id'            => 'contact_page_below_sidebar',
		'name'          => __( 'Contact page below sidebar', 'sg' ),
		'description'   => '',
		'before_widget' => '<div class="four columns">

                                <div class="gdlr-box-with-icon-ux gdlr-ux"
                                     style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                    <div class="gdlr-item gdlr-box-with-icon-item pos-top type-circle">
                                        <div class="box-with-circle-icon" style="background-color: #e2714d"><i
                                                    class="icon-phone-sign fa fa-phone-sign"
                                                    style="color:#ffffff;"></i><br></div>',
		'after_widget'  => ' </div>
                                </div>
                            </div>',
		'before_title'  => '<h4 class="box-with-icon-title">',
		'after_title'   => '</h4>'
	) );
}

/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150 );
}

/**
 * Підключаємо виведення логотипу
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'custom-logo' );
}


/**
 * Добавляємо різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'archive', 400, 300, true );
	add_image_size( 'banner', 350, 160, true );
	add_image_size( 'courses', 700, 400, true );
}


/**
 * Реєструємо формати постів
 */
function add_post_formats() {
	add_theme_support( 'post-formats', array(
		'aside',
		'gallery',
		'link',
		'image',
		'quote',
		'status',
		'video',
		'audio',
		'chat'
	) );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'custom-background' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'custom-logo' );
}

add_action( 'after_setup_theme', 'add_post_formats', 11 );


/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
	return ' ...';
}

add_filter( 'excerpt_more', 'custom_excerpt_more' );


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 30;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/**
 * Підключаємо javascript файли
 */
function add_theme_scripts() {
	wp_enqueue_script( "jquery" );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array( 'jquery', 'bootstrap' ) );
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/**
 * Підключаємо css файли
 */
function add_theme_style() {
	//wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'add_theme_style' );

/**
 * Custom widgets
 */


/**
 * Class PhoneEmailWidget
 *
 * Show address, phone, e-mail, working time.
 * If address and working time is empty show phone and e-mail in row,
 * else in column
 */
class PhoneEmailWidget extends WP_Widget {
	/**
	 * PhoneEmailWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "phone_email_widget", __( "Contacts", "sg" ),
			array( "description" => __( "Show contacts", "sg" ) ) );
	}

	/**
	 * Show form with field in control panel
	 *
	 * @param array $instance
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$title   = "";
		$address = "";
		$phone   = "";
		$email   = "";
		$work    = "";

		if ( ! empty( $instance ) ) {
			$title   = $instance["title"];
			$address = $instance["address"];
			$phone   = $instance["phone"];
			$email   = $instance["email"];
			$work    = $instance["work"];
		}

		$tableId   = $this->get_field_id( "title" );
		$tableName = $this->get_field_name( "title" );
		echo '<label for="' . $tableId . '">' . __( 'Title', 'sg' ) . '</label><br>';
		echo '<input id="' . $tableId . '" type="text" name="' .
		     $tableName . '" value="' . $title . '"><br>';

		$addressId   = $this->get_field_id( "address" );
		$addressName = $this->get_field_name( "address" );
		echo '<label for="' . $addressId . '">' . __( 'Address', 'sg' ) . '</label><br>';
		echo '<input id="' . $addressId . '" type="text" name="' .
		     $addressName . '" value="' . $address . '"><br>';

		$phoneId   = $this->get_field_id( "phone" );
		$phoneName = $this->get_field_name( "phone" );
		echo '<label for="' . $phoneId . '">' . __( 'Phone', 'sg' ) . '</label><br>';
		echo '<input id="' . $phoneId . '" type="text" name="' .
		     $phoneName . '" value="' . $phone . '"><br>';

		$emailId   = $this->get_field_id( "email" );
		$emailName = $this->get_field_name( "email" );
		echo '<label for="' . $emailId . '">' . __( 'E-mail', 'sg' ) . '</label><br>';
		echo '<input id="' . $emailId . '" type="text" name="' .
		     $emailName . '" value="' . $email . '"><br>';

		$workId   = $this->get_field_id( "work" );
		$workName = $this->get_field_name( "work" );
		echo '<label for="' . $workId . '">' . __( 'Time of work', 'sg' ) . '</label><br>';
		echo '<input id="' . $workId . '" type="text" name="' .
		     $workName . '" value="' . $work . '"><br>';
	}

	/**
	 * Update database
	 *
	 * @param array $newInstance
	 * @param array $oldInstance
	 *
	 * @return array
	 */
	public function update( $newInstance, $oldInstance ) {
		$values            = array();
		$values["title"]   = htmlentities( $newInstance["title"] );
		$values["address"] = htmlentities( $newInstance["address"] );
		$values["phone"]   = htmlentities( $newInstance["phone"] );
		$values["email"]   = htmlentities( $newInstance["email"] );
		$values["work"]    = htmlentities( $newInstance["work"] );

		return $values;
	}

	/**
	 * Show instance in site
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget']
		     . $args['before_title']
		     . $instance['title']
		     . $args['after_title']
		     . '<div class="top-navigation-left-text">';
		if ( ! empty( $instance['address'] ) ) {
			echo ''
			     . '<div style="margin: 0px 10px; display: inline-block; *display: inline; *zoom:1;">'
			     . $instance['address']
			     . '</div>';
		}
		echo ''
		     . '<div style="margin: 0px 10px; display: inline-block; *display: inline; *zoom:1;">'
		     . '<i class="gdlr-icon icon-phone fa fa-phone" style="color: #bababa; font-size: 14px; "></i>'
		     . $instance['phone']
		     . '</div>'

		     . '<div style="margin: 0px 10px ; display: inline-block; *display: inline;  *zoom:1;">'
		     . '<i class="gdlr-icon icon-envelope fa fa-envelope" style="color: #bababa; font-size: 14px; "></i>'
		     . $instance['email']
		     . '</div>';
		if ( ! empty( $instance['work'] ) ) {
			echo ''
			     . '<div style="margin: 0px 10px; display: inline-block; *display: inline; *zoom:1;">'
			     . $instance['work']
			     . '</div>';
		}
		echo ''
		     . '</div>'
		     . $args['after_widget'];
	}

}

add_action( "widgets_init", function () {
	register_widget( "PhoneEmailWidget" );
} );


/**
 * Class FeedbackWidget
 */
class FeedbackWidget extends WP_Widget {
	/**
	 * FeedbackWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "feedback_widget", __( "Feedback", "sg" ),
			array( "description" => __( "Show form feedback", "sg" ) ) );
	}

	/**
	 * Show form with field in control panel
	 *
	 * @param array $instance
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$title = "";

		if ( ! empty( $instance ) ) {
			$title = $instance["title"];
		}

		$tableId   = $this->get_field_id( "title" );
		$tableName = $this->get_field_name( "title" );
		echo '<label for="' . $tableId . '">' . __( 'Title', 'sg' ) . '</label><br>';
		echo '<input id="' . $tableId . '" type="text" name="' .
		     $tableName . '" value="' . $title . '"><br>';
	}

	/**
	 * Update database
	 *
	 * @param array $newInstance
	 * @param array $oldInstance
	 *
	 * @return array
	 */
	public function update( $newInstance, $oldInstance ) {
		$values          = array();
		$values["title"] = htmlentities( $newInstance["title"] );

		return $values;
	}

	/**
	 * Show instance in site
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget']
		     . $args['before_title']
		     . $instance['title']
		     . $args['after_title'];
		?>
        <div class="gdlr-space" style="margin-top: 45px;"></div>
        <div role="form" class="wpcf6" id="wpcf7-f3444-o1" lang="en-US" dir="ltr">
            <div class="screen-reader-response"></div>
            <form method="post"
                  class="wpcf7-form" novalidate="novalidate">
                <div style="display: none;">
                    <input type="hidden" name="_wpcf7" value="3444">
                    <input type="hidden" name="_wpcf7_version" value="4.7">
                    <input type="hidden" name="_wpcf7_locale" value="en_US">
                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3444-o1">
                    <input type="hidden" name="_wpnonce" value="c71e32297f">
                </div>
                <p>Your Name (required)
                    <br>
                    <span class="wpcf7-form-control-wrap your-name">
                        <input
                                type="text" name="your-name" value="" size="40"
                                class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                aria-required="true" aria-invalid="false">
                    </span>
                </p>
                <p>Your Email (required)
                    <br>
                    <span class="wpcf7-form-control-wrap your-email">
                        <input
                                type="email" name="your-email" value="" size="40"
                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                aria-required="true" aria-invalid="false">
                    </span>
                </p>
                <p>Subject<br>
                    <span class="wpcf7-form-control-wrap your-subject">
                        <input
                                type="text" name="your-subject" value="" size="40"
                                class="wpcf7-form-control wpcf7-text"
                                aria-invalid="false">
                    </span>
                </p>
                <p>Your Message<br>
                    <span class="wpcf7-form-control-wrap your-message">
                        <textarea
                                name="your-message" cols="40" rows="10"
                                class="wpcf7-form-control wpcf7-textarea"
                                aria-invalid="false">
                        </textarea>
                    </span>
                </p>
                <p>
                    <input type="submit" value="Send" name="submit"
                           class="wpcf7-form-control wpcf7-submit">
                    <span
                            class="ajax-loader">

                    </span>
                </p>
                <div class="wpcf7-response-output wpcf7-display-none"></div>
            </form>
        </div>
		<?php
		echo ''
		     . $args['after_widget'];

		if ( isset( $_POST['submit'] ) ) {
			$hasError  = false;
			$nameError = "";

			$name = "";
			if ( trim( $_POST['your-name'] ) === '' ) {
				$nameError = __( 'Enter your name.', 'sg' );
				$hasError  = true;
			} else {
				$name = trim( $_POST['contactName'] );
			}

			$email = "";
			if ( trim( $_POST['your-email'] ) === "" ) {
				$nameError = $nameError . __( ' Enter your e-mail.', 'sg' );
				$hasError  = true;
			} else if ( ! is_email( $_POST['your-email'] ) ) {
				$nameError = $nameError . __( ' You enter wrong e-mail.', 'sg' );
				$hasError  = true;
			} else {
				$email = trim( $_POST['email'] );
			}

			$subject = trim( $_POST['your-subject'] );

			$message = "";
			if ( trim( $_POST['your-message'] ) === "" ) {
				$nameError = $nameError . __( ' Enter your message.', 'sg' );
				$hasError  = true;
			} else {
				if ( function_exists( 'stripslashes' ) ) {
					$message = stripslashes( trim( $_POST['your-message'] ) );
				} else {
					$message = trim( $_POST['your-message'] );
				}
			}
			if ( ! $hasError ) {

				$to        = get_option( 'admin_email' );
				$headers[] = 'From: ' . $name . '<' . $email . '>';
				$headers[] = "Content-type: text/plain; charset=\"utf-8\"";

				remove_all_filters( 'wp_mail_from' );
				remove_all_filters( 'wp_mail_from_name' );

				if ( wp_mail( $to, $subject, $message ) ) {//}, $headers )) {
					$msg = __( 'You message was sent', 'sg' );
					echo "<script>alert('$msg' );</script>";
				} else {
					$msg = __( 'Sorry. You message was not sent', 'sg' );
					echo "<script>alert('$msg' );</script>";
				}
			} else {
				echo "<script>alert( '$nameError' );</script>";
			}

		}

	}
}

add_action( "widgets_init", function () {
	register_widget( "FeedbackWidget" );
} );


/**
 * Class SocialShareWidget
 */
class SocialShareWidget extends WP_Widget {
	/**
	 * SocialShareWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "social_share_widget", __( "Social share", "sg" ),
			array( "description" => __( "Show social share button", "sg" ) ) );
	}

	/**
	 * Show form with field in control panel
	 *
	 * @param array $instance
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$title       = '';
		$facebook    = '';
		$flickr      = '';
		$google_plus = '';
		$linkedin    = '';
		$tumblr      = '';
		$twitter     = '';
		$vimeo       = '';

		if ( ! empty( $instance ) ) {
			$title       = $instance["title"];
			$facebook    = $instance["facebook"];
			$flickr      = $instance["flickr"];
			$google_plus = $instance["google_plus"];
			$linkedin    = $instance["linkedin"];
			$tumblr      = $instance["tumblr"];
			$twitter     = $instance["twitter"];
			$vimeo       = $instance["vimeo"];
		}

		$tableId   = $this->get_field_id( "title" );
		$tableName = $this->get_field_name( "title" );
		echo '<label for="' . $tableId . '">' . __( 'Title', 'sg' ) . '</label><br>';
		echo '<input id="' . $tableId . '" type="text" name="' .
		     $tableName . '" value="' . $title . '"><br>';

		$facebookId   = $this->get_field_id( "facebook" );
		$facebookName = $this->get_field_name( "facebook" );
		echo '<label for="' . $facebookId . '">Facebook  </label>';
		echo '<input id="' . $facebookId . '" type="checkbox" name="' .
		     $facebookName . '" value="checked"' . $facebook . '><br>';

		$flickrId   = $this->get_field_id( "flickr" );
		$flickrName = $this->get_field_name( "flickr" );
		echo '<label for="' . $flickrId . '">Flickr  </label>';
		echo '<input id="' . $flickrId . '" type="checkbox" name="' .
		     $flickrName . '" value="checked"' . $flickr . '><br>';

		$googlePlusId   = $this->get_field_id( "google_plus" );
		$googlePlusName = $this->get_field_name( "google_plus" );
		echo '<label for="' . $googlePlusId . '">Google plus  </label>';
		echo '<input id="' . $googlePlusId . '" type="checkbox" name="' .
		     $googlePlusName . '" value="checked"' . $google_plus . '><br>';

		$linkedinId   = $this->get_field_id( "linkedin" );
		$linkedinName = $this->get_field_name( "linkedin" );
		echo '<label for="' . $linkedinId . '">Linkedin  </label>';
		echo '<input id="' . $linkedinId . '" type="checkbox" name="' .
		     $linkedinName . '" value="checked"' . $linkedin . '><br>';

		$tumblrId   = $this->get_field_id( "tumblr" );
		$tumblrName = $this->get_field_name( "tumblr" );
		echo '<label for="' . $tumblrId . '">Tumblr  </label>';
		echo '<input id="' . $tumblrId . '" type="checkbox" name=' .
		     $tumblrName . '" value="checked"' . $tumblr . '><br>';

		$twitterId   = $this->get_field_id( "twitter" );
		$twitterName = $this->get_field_name( "twitter" );
		echo '<label for="' . $twitterId . '">Twitter  </label>';
		echo '<input id="' . $twitterId . '" type="checkbox" name="' .
		     $twitterName . '" value="checked"' . $twitter . '><br>';

		$vimeoId   = $this->get_field_id( "vimeo" );
		$vimeoName = $this->get_field_name( "vimeo" );
		echo '<label for="' . $vimeoId . '">Vimeo  </label>';
		echo '<input id="' . $vimeoId . '" type="checkbox" name="' .
		     $vimeoName . '" value="checked"' . $vimeo . '><br>';

	}

	/**
	 * Update database
	 *
	 * @param array $newInstance
	 * @param array $oldInstance
	 *
	 * @return array
	 */
	public function update( $newInstance, $oldInstance ) {
		$values                = array();
		$values["title"]       = htmlentities( $newInstance["title"] );
		$values["facebook"]    = htmlentities( $newInstance["facebook"] );
		$values["flickr"]      = htmlentities( $newInstance["flickr"] );
		$values["google_plus"] = htmlentities( $newInstance["google_plus"] );
		$values["linkedin"]    = htmlentities( $newInstance["linkedin"] );
		$values["tumblr"]      = htmlentities( $newInstance["tumblr"] );
		$values["twitter"]     = htmlentities( $newInstance["twitter"] );
		$values["vimeo"]       = htmlentities( $newInstance["vimeo"] );

		return $values;
	}

	/**
	 * Show instance in site
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		echo ''
		     . $args['before_widget']
		     . $args['before_title']
		     . $instance['title']
		     . $args['after_title']//             . '<div class="top-social-wrapper">'
		;
		if ( 'checked' == $instance['facebook'] ) {
			echo ''

			     . '<div class="social-icon gdlr-icon  fa " >'
			     . '        <a href = "#" target = "_blank" >'
			     . '           <img width = "32" height = "32" src = "http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/images/dark/social-icon/facebook.png" alt = "Facebook" >'
			     . '        </a >'
			     . '   </div >';
		};
		if ( 'checked' == $instance['flickr'] ) {
			echo ''
			     . '   <div class="social-icon gdlr-icon  fa ">'
			     . '       <a href="#" target="_blank">'
			     . '            <img width="32" height="32" src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/images/dark/social-icon/flickr.png" alt="Flickr">'
			     . '       </a>'
			     . '   </div>';
		};
		if ( 'checked' == $instance['google_plus'] ) {
			echo ''
			     . '   <div class="social-icon gdlr-icon  fa">'
			     . '       <a href="#" target="_blank">'
			     . '            <img width="32" height="32" src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/images/dark/social-icon/google-plus.png" alt="Google Plus">'
			     . '       </a>'
			     . '   </div>';
		};
		if ( 'checked' == $instance['linkedin'] ) {
			echo ''
			     . '   <div class="social-icon gdlr-icon  fa">'
			     . '       <a href="#" target="_blank">'
			     . '           <img width="32" height="32" src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/images/dark/social-icon/linkedin.png" alt="Linkedin">'
			     . '        </a>'
			     . '   </div>';
		};
		if ( 'checked' == $instance['tumblr'] ) {
			echo ''
			     . '    <div class="social-icon gdlr-icon  fa">'
			     . '       <a href="#" target="_blank">'
			     . '           <img width="32" height="32" src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/images/dark/social-icon/tumblr.png" alt="Tumblr">'
			     . '       </a>'
			     . '   </div>';
		};
		if ( 'checked' == $instance['twitter'] ) {
			echo ''
			     . '   <div class="social-icon gdlr-icon  fa">'
			     . '       <a href="#" target="_blank">'
			     . '           <img width="32" height="32" src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/images/dark/social-icon/twitter.png" alt="Twitter">'
			     . '       </a>'
			     . '   </div>';
		};
		if ( 'checked' == $instance['vimeo'] ) {
			echo ''
			     . '   <div class="social-icon gdlr-icon  fa">'
			     . '       <a href="#" target="_blank">'
			     . '            <img width="32" height="32" src="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/themes/clevercourse/images/dark/social-icon/vimeo.png" alt="Vimeo">'
			     . '       </a>'
			     . '   </div>';
		};
		echo ''
		     //             . '</div>'
		     . $args['after_widget'];
	}

}

add_action( "widgets_init", function () {
	register_widget( "SocialShareWidget" );
} );

/**
 * Class LoginWidget
 */
class LoginWidget extends WP_Widget {
	/**
	 * LoginWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "login_widget", __( "Login form", "sg" ),
			array( "description" => __( "Open login form", "sg" ) ) );
	}

	/**
	 * Show form in site
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		echo ''
		     . '<div class="gdlr-lms-header-signin">'
		     . '    <i class="fa fa-lock icon-lock"></i>'
		     . '    <a data-rel="gdlr-lms-lightbox" data-lb-open="login-form">Sign In</a>'
		     . '    <div class="gdlr-lms-lightbox-container login-form">'
		     . '         <div class="gdlr-lms-lightbox-close">'
		     . '             <i class="fa fa-remove icon-remove"></i>'
		     . '         </div>'
		     . '         <h3 class="gdlr-lms-lightbox-title">Please sign in first</h3>';
		echo '<form class="gdlr-lms-form gdlr-lms-lightbox-form" id="loginform" method="post"
                  action="https://demo.goodlayers.com/clevercourse/wp-login.php">
                <p class="gdlr-lms-half-left">
                    <span>Username</span>
                    <input type="text" name="log">
                </p>
                <p class="gdlr-lms-half-right">
                    <span>Password</span>
                    <input type="password" name="pwd">
                </p>
                <div class="clear"></div>
                <p class="gdlr-lms-lost-password">
                    <a href="https://demo.goodlayers.com/clevercourse/wp-login.php?action=lostpassword&amp;redirect_to=http://demo.goodlayers.com/clevercourse">
                        Lost Your Password?</a>
                </p>
                <p>
                    <input type="hidden" name="home_url"
                           value="http://demo.goodlayers.com/clevercourse">
                    <input type="hidden" name="rememberme" value="forever">
                    <input type="hidden" name="redirect_to" value="/clevercourse/">
                    <input type="submit" name="wp-submit" class="gdlr-lms-button" value="Sign In!">
                </p>
            </form>
            <h3 class="gdlr-lms-lightbox-title second-section">Not a member?</h3>
            <div class="gdlr-lms-lightbox-description">Please simply create an account before
                buying/booking any courses.
            </div>
            <a class="gdlr-lms-button blue"
               href="http://demo.goodlayers.com/clevercourse?register=http://demo.goodlayers.com/clevercourse/">
                Create an account for free!
            </a>'
		     . '    </div>'
		     . '    <span class="gdlr-separator">|</span>'
		     . '    <a href="http://demo.goodlayers.com/clevercourse?register=http://demo.goodlayers.com/clevercourse/">Sign Up</a>'
		     . '</div>';
	}
}

add_action( "widgets_init", function () {
	register_widget( "LoginWidget" );
} );

/**
 * Class CopyrightWidget
 */
class CopyrightWidget extends WP_Widget {
	/**
	 * CopyrightWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "copyright_widget", __( "Copyright text", "sg" ),
			array( "description" => __( "Show copyright text", "sg" ) ) );
	}

	/**
	 * Show form with field in control panel
	 *
	 * @param array $instance
	 *
	 * @return void
	 */
	public function form( $instance ) {
		$left  = "";
		$right = "";

		if ( ! empty( $instance ) ) {
			$left  = $instance["left"];
			$right = $instance["right"];
		}

		$leftId   = $this->get_field_id( "left" );
		$leftName = $this->get_field_name( "left" );
		echo '<label for="' . $leftId . '">' . __( 'Left side', 'sg' ) . '</label><br>';
		echo '<input id="' . $leftId . '" type="text" name="' .
		     $leftName . '" value="' . $left . '"><br>';

		$rightId   = $this->get_field_id( "right" );
		$rightName = $this->get_field_name( "right" );
		echo '<label for="' . $rightId . '">' . __( 'Right side', 'sg' ) . '</label><br>';
		echo '<input id="' . $rightId . '" type="text" name="' .
		     $rightName . '" value="' . $right . '"><br>';
	}

	/**
	 * Update database
	 *
	 * @param array $newInstance
	 * @param array $oldInstance
	 *
	 * @return array
	 */
	public function update( $newInstance, $oldInstance ) {
		$values          = array();
		$values["left"]  = htmlentities( $newInstance["left"] );
		$values["right"] = htmlentities( $newInstance["right"] );

		return $values;
	}

	/**
	 * Show instance in site
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget']
		     . $args['before_title']
		     . $args['after_title'];
		echo '<div class="copyright-left">';
		echo $instance['left'];
		echo '</div>';
		echo '<div class="copyright-right">';
		echo $instance['right'];
		echo '</div>';
		echo '<div class="clear"></div>';
		echo $args['after_widget'];
	}
}

add_action( "widgets_init", function () {
	register_widget( "CopyrightWidget" );
} );


/**
 * Class SmallPostWidget
 */
class SmallPostWidget extends WP_Widget {
	/**
	 * SmallPostWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "small_post_widget", __( "Small post", "sg" ),
			array( "description" => __( "Show 4 Small post", "sg" ) ) );
	}

	public function form( $instance ) {
	}

	public function update( $newInstance, $oldInstance ) {
	}

	/**
	 * Show instance in site
	 *
	 */
	public function widget( $args, $instance ) {
		$args        = array( 'numberposts' => 4, 'post_type' => 'small_post', 'order' => 'ASC', 'orderby' => 'date' );
		$small_posts = get_posts( $args );
		?>
        <div class="gdlr-color-wrapper  gdlr-show-all no-skin"
             style="background-color: #ffffff; padding-top: 75px; padding-bottom: 45px; ">
            <div class="container">
				<?php foreach ( $small_posts as $small_post ) { ?>
                    <div class="three columns">
                        <div class="gdlr-ux column-service-ux">
                            <div class="gdlr-item gdlr-column-service-item gdlr-type-2">
                                <div class="column-service-image">
									<?php echo get_the_post_thumbnail( $small_post->ID, 'thumbnail' ); ?>
                                </div>
                                <div class="column-service-content-wrapper">
                                    <h3 class="column-service-title">
										<?php echo $small_post->post_title ?>
                                    </h3>
                                    <div class="column-service-content gdlr-skin-content">
                                        <p>
											<?php echo $small_post->post_content ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

				<?php } ?>
                <div class="clear"></div>
            </div>

        </div>
		<?php
	}
}

add_action( "widgets_init", function () {
	register_widget( "SmallPostWidget" );
} );

/**
 * Class CoursesPostWidget
 */
class CoursesPostWidget extends WP_Widget {
	/**
	 * CoursesPostWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "courses_post_widget", __( "Courses post", "sg" ),
			array( "description" => __( "Show Courses post", "sg" ) ) );
	}

	public function form( $instance ) {
	}

	public function update( $newInstance, $oldInstance ) {
	}

	/**
	 * Show instance in site
	 *
	 */
	public function widget( $args, $instance ) {

		$args          = array( 'numberposts' => 10, 'post_type' => 'courses', 'order' => 'ASC', 'orderby' => 'date' );
		$courses_posts = get_posts( $args );
		?>

        <section id="content-section-4">
            <div class="gdlr-color-wrapper  gdlr-show-all no-skin"
                 style="background-color: #ffffff; padding-top: 90px; padding-bottom: 60px; ">
                <div class="container">
                    <div class="gdlr-item-title-wrapper gdlr-item pos-center gdlr-nav-container ">
                        <div class="gdlr-item-title-head">
                            <i class="fa fa-angle-left icon-angle-left gdlr-flex-prev"></i>
                            <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">Recent Courses</h3>
                            <i class="fa fa-angle-right icon-angle-right gdlr-flex-next"></i>
                            <div class="clear"></div>
                        </div>

                    </div>
                    <div class="course-item-wrapper">
                        <div class="gdlr-lms-course-grid2-wrapper gdlr-lms-carousel">
                            <div class="flexslider" data-type="carousel" data-nav-container="course-item-wrapper"
                                 data-columns="3">
                                <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                    <ul class="slides" style="width: 1400%; margin-left: 0px;">
										<?php foreach ( $courses_posts as $courses_post ) { ?>
                                            <li class="gdlr-lms-course-grid2 gdlr-lms-item gdlr-lms-free"
                                                style="width: 360px; float: left; display: block; height: 381px;">
                                                <div class="gdlr-lms-course-thumbnail">
													<?php echo get_the_post_thumbnail( $courses_post->ID, 'courses' ); ?>
                                                </div>
                                                <div class="gdlr-lms-course-content">
                                                    <h3 class="gdlr-lms-course-title">
                                                        <a href="<?php echo get_permalink( $courses_post->ID ); ?>">
															<?php echo $courses_post->post_title ?>

                                                        </a>
                                                    </h3>
                                                    <div class="gdlr-lms-course-price">
                                                    <span class="price-button blue">
                                                        <?php echo get_post_custom_values( 'price', $courses_post->ID )[0]; ?>

                                                    </span>
                                                    </div>
                                                    <div class="gdlr-lms-course-info">
                                                        <i class="fa fa-clock-o icon-time"></i>
                                                        <span class="tail">
                                                        <?php echo get_post_custom_values( 'info', $courses_post->ID )[0]; ?>
                                                    </span>
                                                    </div>
                                                    <div class="clear">

                                                    </div>
                                                </div>
                                            </li>

										<?php } ?>
                                        <script>
                                            var myIndexCourse = 0;
                                            carouselCourse();

                                            showDivsCourse(myIndexCourse);

                                            function plusDivsCourse(n) {
                                                showDivsCourse(myIndexCourse += n);
                                            }

                                            function showDivsCourse(n) {
                                                var i;
                                                var x = document.getElementsByClassName("gdlr-lms-course-grid2 gdlr-lms-item gdlr-lms-free");
                                                if (n > x.length) {
                                                    myIndexCourse = 1
                                                }
                                                if (n < 1) {
                                                    myIndexCourse = x.length
                                                }
                                                for (i = 0; i < x.length; i++) {
                                                    x[i].style.display = "none";
                                                }
                                                x[myIndexCourse - 1].style.display = "block";
                                            }

                                            function carouselCourse() {
                                                var i;
                                                var x = document.getElementsByClassName("gdlr-lms-course-grid2 gdlr-lms-item gdlr-lms-free");
                                                for (i = 0; i < x.length; i++) {
                                                    x[i].style.display = "none";
                                                }
                                                myIndexCourse++;
                                                if (myIndexCourse > x.length) {
                                                    myIndexCourse = 1
                                                }
                                                if ((myIndexCourse - 1) <= x.length) {
                                                    x[myIndexCourse - 1].style.display = "block";
                                                }
                                                myIndexCourse++;
                                                if (myIndexCourse > x.length) {
                                                    myIndexCourse = 1
                                                }
                                                if ((myIndexCourse - 1) <= x.length) {
                                                    x[myIndexCourse - 1].style.display = "block";
                                                }
                                                myIndexCourse++;
                                                if (myIndexCourse > x.length) {
                                                    myIndexCourse = 1
                                                }
                                                if ((myIndexCourse - 1) <= x.length) {
                                                    x[myIndexCourse - 1].style.display = "block";
                                                }

                                                setTimeout(carouselCourse, 2500);
                                            }
                                        </script>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </section>

		<?php
	}
}

add_action( "widgets_init", function () {
	register_widget( "CoursesPostWidget" );
} );

/**
 * Class SearchForCourses
 */
class SearchForCourses extends WP_Widget {
	/**
	 * SearchForCourses constructor.
	 */
	public function __construct() {
		parent::__construct( "search_for_courses_widget", __( "Search for courses", "sg" ),
			array( "description" => __( "Search for courses", "sg" ) ) );
	}

	public function form( $instance ) {
	}

	public function update( $newInstance, $oldInstance ) {
	}

	/**
	 * Show instance in site
	 *
	 */
	public function widget( $args, $instance ) {
		$args          = array( 'post_type' => 'courses', 'order' => 'ASC', 'orderby' => 'date' );
		$courses_posts = get_posts( $args );

		?>

        <section id="content-section-5">
            <div class="gdlr-parallax-wrapper gdlr-background-image gdlr-show-all gdlr-skin-dark-skin"
                 id="gdlr-parallax-wrapper-1" data-bgspeed="0"
                 style="background-image:
                 url('http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/uploads/2014/08/search-bg.jpg'); padding-top: 130px; padding-bottom: 90px; ">
                <div class="container">
                    <div class="gdlr-item-title-wrapper gdlr-item pos-center ">
                        <div class="gdlr-item-title-head">
                            <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">
                                Search For Courses
                            </h3>
                            <div class="clear"></div>
                        </div>
                        <div class="gdlr-item-title-divider"></div>
                        <div class="gdlr-item-title-caption gdlr-skin-info">
                            Fill keywords to seek for courses
                        </div>
                    </div>
                    <div class="course-search-wrapper">
                        <form class="gdlr-lms-form" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <div class="course-search-column gdlr-lms-1">
	                        	<span class="gdlr-lms-combobox">
	                        		<select name="course_category">
				                        <option value="">Category</option>
				                        <?php
				                        $terms = get_terms(array('taxonomy' => 'course_category'));
				                        foreach ($terms as $term){
					                        echo '<option value="' . $term->slug . '">' . $term->name . '</option>';
				                        }
				                        ?>
                                    </select>
	                        	</span>
                            </div>
                            <div class="course-search-column gdlr-lms-2">
                        		<span class="gdlr-lms-combobox">
			                        <select name="course_type" id="gender">
				                        <option value="">Type</option>
                                        <?php
                                            $terms = get_terms(array('taxonomy' => 'course_type'));
		                                    foreach ($terms as $term){
                                            echo '<option value="' . $term->slug . '">' . $term->name . '</option>';
                                            }
                                        ?>
			                        </select>
		                        </span>
                            </div>
                            <div class="course-search-column gdlr-lms-3">
                                <input type="text" name="s" id="s" value=""
                                       autocomplete="off" placeholder="Keywords&hellip;">
                            </div>
                            <div class="course-search-column gdlr-lms-4">
                                <input type="hidden" name="post_type" value="courses<?php echo get_search_query() ?>">
                                <input class="gdlr-lms-button" type="submit" id="searchsubmit" value="Search!">
                            </div>
                            <div class="clear"></div>
                        </form>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </section>

		<?php
	}
}

add_action( "widgets_init", function () {
	register_widget( "SearchForCourses" );
} );

/**
 * Class StudentSayWidget
 */
class StudentSayWidget extends WP_Widget {
	/**
	 * StudentSayWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "student_say_widget", __( "Student say posts", "sg" ),
			array( "description" => __( "Show Student say posts", "sg" ) ) );
	}

	public function form( $instance ) {
	}

	public function update( $newInstance, $oldInstance ) {
	}

	/**
	 * Show instance in site
	 *
	 */
	public function widget( $args, $instance ) {

		$args         = array(
			'numberposts' => 10,
			'post_type'   => 'student_say',
			'order'       => 'ASC',
			'orderby'     => 'date'
		);
		$student_says = get_posts( $args );
		?>

        <section id="content-section-7">
            <div class="gdlr-color-wrapper  gdlr-show-all no-skin"
                 style="background-color: #f5f5f5; padding-top: 85px; padding-bottom: 45px; ">
                <div class="container">
                    <div class="gdlr-testimonial-item-wrapper">
                        <div class="gdlr-item-title-wrapper gdlr-item pos-center gdlr-nav-container ">
                            <div class="gdlr-item-title-head">
                                <button class="fa fa-angle-left icon-angle-left gdlr-flex-prev"
                                        onclick="plusDivsStudent(-1)"></button>
                                <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">What Student Say?</h3>
                                <button class="fa fa-angle-right icon-angle-right gdlr-flex-next"
                                        onclick="plusDivsStudent(1)"></button>
                                <div class="clear">

                                </div>
                            </div>
                            <div class="gdlr-item-title-divider"></div>
                        </div>
                        <div class="gdlr-item gdlr-testimonial-item carousel plain-style">
                            <div class="gdlr-ux gdlr-testimonial-ux"
                                 style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                                <div class="flexslider" data-type="carousel" data-nav-container="gdlr-testimonial-item"
                                     data-columns="1">
                                    <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                        <ul class="slides" style="width: 600%; margin-left: 0px;">
											<?php foreach ( $student_says as $student_say ) { ?>
                                                <li class="testimonial-item"
                                                    style="width: 1110px; float: left; display: block;">
                                                    <div class="testimonial-content-wrapper">
														<?php echo $student_say->post_content ?>
                                                    </div>
                                                </li>
											<?php } ?>
                                            <script>
                                                var myIndexStudent = 0;
                                                carouselStudent();

                                                showDivsStudent(myIndexStudent);

                                                function plusDivsStudent(n) {
                                                    showDivsStudent(myIndexStudent += n);
                                                }

                                                function showDivsStudent(n) {
                                                    var i;
                                                    var x = document.getElementsByClassName("testimonial-item");
                                                    if (n > x.length) {
                                                        myIndexStudent = 1
                                                    }
                                                    if (n < 1) {
                                                        myIndexStudent = x.length
                                                    }
                                                    for (i = 0; i < x.length; i++) {
                                                        x[i].style.display = "none";
                                                    }
                                                    x[myIndexStudent - 1].style.display = "block";
                                                }

                                                function carouselStudent() {
                                                    var i;
                                                    var x = document.getElementsByClassName("testimonial-item");
                                                    for (i = 0; i < x.length; i++) {
                                                        x[i].style.display = "none";
                                                    }
                                                    myIndexStudent++;
                                                    if (myIndexStudent > x.length) {
                                                        myIndexStudent = 1
                                                    }
                                                    x[myIndexStudent - 1].style.display = "block";
                                                    setTimeout(carouselStudent, 2500);
                                                }
                                            </script>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">

                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </section>

		<?php
	}
}

add_action( "widgets_init", function () {
	register_widget( "StudentSayWidget" );
} );


/**
 * Class CoursesBannerWidget
 */
class CoursesBannerWidget extends WP_Widget {
	/**
	 * CoursesBannerWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "courses_banner_widget", __( "Courses banner", "sg" ),
			array( "description" => __( "Show Courses banner", "sg" ) ) );
	}

	public function form( $instance ) {
	}

	public function update( $newInstance, $oldInstance ) {
	}

	/**
	 * Show instance in site
	 *
	 */
	public function widget( $args, $instance ) {

		$args            = array(
			'numberposts' => 3,
			'post_type'   => 'courses_banner',
			'order'       => 'ASC',
			'orderby'     => 'date'
		);
		$courses_banners = get_posts( $args );
		?>

        <section id="content-section-8">
            <div class="gdlr-color-wrapper  gdlr-show-all no-skin"
                 style="background-color: #ffffff; padding-top: 85px; padding-bottom: 60px; ">
                <div class="container">
                    <div class="gdlr-title-item" style="margin-bottom: 50px;">
                        <div class="gdlr-item-title-wrapper gdlr-item pos-center ">
                            <div class="gdlr-item-title-head">
                                <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">Check out some Popular
                                    courses</h3>
                                <div class="clear"></div>
                            </div>
                            <div class="gdlr-item-title-divider"></div>
                            <div class="gdlr-item-title-caption gdlr-skin-info">Tellus Elit Euismod Ultricies
                            </div>
                        </div>
                    </div>
                    <div class="gdlr-banner-item-wrapper">
                        <div class="gdlr-banner-images gdlr-item" style="margin-bottom: 0px;">
                            <div class="flexslider" data-pausetime="7000" data-slidespeed="600" data-effect="fade"
                                 data-columns="3" data-type="carousel" data-nav-container="gdlr-banner-images">
                                <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                    <ul class="slides" style="width: 600%; margin-left: 0px;">
										<?php foreach ( $courses_banners as $courses_banner ) { ?>
                                            <li style="width: 350px; float: left; display: block;">
												<?php echo get_the_post_thumbnail( $courses_banner->ID, 'banner' ); ?>
                                            </li>
										<?php } ?>
                                    </ul>
                                </div>
                                <ul class="flex-direction-nav">
                                    <li><a class="flex-prev flex-disabled" href="#" tabindex="-1"><i
                                                    class="icon-angle-left"></i></a></li>
                                    <li><a class="flex-next flex-disabled" href="#" tabindex="-1"><i
                                                    class="icon-angle-right"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clear">

                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
        </section>
		<?php
	}
}

add_action( "widgets_init", function () {
	register_widget( "CoursesBannerWidget" );
} );

/**
 * Class TwitterPostWidget
 */
class TwitterPostWidget extends WP_Widget {
	/**
	 * TwitterPostWidget constructor.
	 */
	public function __construct() {
		parent::__construct( "twitter_post_widget", __( "Twitter post", "sg" ),
			array( "description" => __( "Show Twitter post", "sg" ) ) );
	}

	public function form( $instance ) {
	}

	public function update( $newInstance, $oldInstance ) {
	}

	/**
	 * Show instance in site
	 *
	 */
	public function widget( $args, $instance ) {

		$args          = array( 'numberposts' => 10, 'post_type' => 'twitter', 'order' => 'ASC', 'orderby' => 'date' );
		$twitter_posts = get_posts( $args );
		?>

        <section id="content-section-9">
            <div class="gdlr-color-wrapper  gdlr-show-all gdlr-skin-green-twitter"
                 style="background-color: #72d5cd; padding-top: 50px; padding-bottom: 20px; ">
                <div class="container">
                    <div class="gdlr-item-title-wrapper gdlr-item pos-center gdlr-twitter-title gdlr-nav-container ">
                        <div class="gdlr-item-title-head">
                            <button class="fa fa-angle-left icon-angle-left gdlr-flex-prev owl-prev icon-chevron-left"
                                    onclick="plusDivsTwitter(-1)"></button>
                            <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">
                                <i class="fa fa-twitter icon-twitter"></i
                            </h3>
                            <button class="fa fa-angle-right icon-angle-right gdlr-flex-next"
                                    onclick="plusDivsTwitter(1)"></button>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="gdlr-item gdlr-twitter-item">
                        <div class="flexslider" data-type="carousel" data-columns="1"
                             data-nav-container="gdlr-twitter-item">
                            <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                <ul class="slides" style="width: 600%; margin-left: 0px;">
									<?php foreach ( $twitter_posts as $twitter_post ) { ?>
                                        <li style="width: 1110px; float: left; display: block;">
                                            <div class="gdlr-twitter">
												<?php echo $twitter_post->post_content ?>
                                            </div>
                                        </li>
									<?php } ?>
                                    <script>
                                        var myIndexTwitter = 0;
                                        carouselTwitter();

                                        showDivsTwitter(myIndexTwitter);

                                        function plusDivsTwitter(n) {
                                            showDivsTwitter(myIndexTwitter += n);
                                        }

                                        function showDivsTwitter(n) {
                                            var i;
                                            var x = document.getElementsByClassName("gdlr-twitter");
                                            if (n > x.length) {
                                                myIndexTwitter = 1
                                            }
                                            if (n < 1) {
                                                myIndexTwitter = x.length
                                            }
                                            for (i = 0; i < x.length; i++) {
                                                x[i].style.display = "none";
                                            }
                                            x[myIndexTwitter - 1].style.display = "block";
                                        }

                                        function carouselTwitter() {
                                            var i;
                                            var x = document.getElementsByClassName("gdlr-twitter");
                                            for (i = 0; i < x.length; i++) {
                                                x[i].style.display = "none";
                                            }
                                            myIndexTwitter++;
                                            if (myIndexTwitter > x.length) {
                                                myIndexTwitter = 1
                                            }
                                            x[myIndexTwitter - 1].style.display = "block";
                                            setTimeout(carouselTwitter, 2500);
                                        }
                                    </script>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </section>

		<?php
	}
}

add_action( "widgets_init", function () {
	register_widget( "TwitterPostWidget" );
} );

/**
 * Add type post Twitter
 */
add_action( 'init', 'true_register_post_type_init' ); // Использовать функцию только внутри хука init
function true_register_post_type_init() {
	$labels = array(
		'name'               => 'Twitter',
		'singular_name'      => 'Twitter', // админ панель Добавить->Функцию
		'add_new'            => __( 'Add new post twitter', 'sg' ),
		'add_new_item'       => __( 'Add new post twitter', 'sg' ), // заголовок тега <title>
		'edit_item'          => __( 'Edit post twitter', 'sg' ),
		'new_item'           => __( 'New post twitter', 'sg' ),
		'all_items'          => __( 'All post twitter', 'sg' ),
		'view_item'          => __( 'View post twitter on site', 'sg' ),
		'search_items'       => __( 'Search post twitter', 'sg' ),
		'not_found'          => __( 'Post twitter not found', 'sg' ),
		'not_found_in_trash' => __( 'Post twitter not found in trash', 'sg' ),
		'menu_name'          => 'Twitter' // ссылка в меню в админке
	);
	$args   = array(
		'labels'        => $labels,
		'public'        => true,
		'show_ui'       => true, // показывать интерфейс в админке
		'has_archive'   => true,
		'menu_icon'     => 'dashicons-twitter', // иконка в меню
		'menu_position' => 4, // порядок в меню
		'supports'      => array( 'title', 'editor', 'author' )//, 'thumbnail'
	);
	register_post_type( 'twitter', $args );
	flush_rewrite_rules( false );
}

/**
 * Add type post Courses
 */
add_action( 'init', 'courses_register_post_type_init' );
function courses_register_post_type_init() {
	$labels = array(
		'name'               => __( 'Courses', 'sg' ),
		'singular_name'      => __( 'Courses', 'sg' ),
		'add_new'            => __( 'Add new post courses', 'sg' ),
		'add_new_item'       => __( 'Add new post courses', 'sg' ),
		'edit_item'          => __( 'Edit post courses', 'sg' ),
		'new_item'           => __( 'New post courses', 'sg' ),
		'all_items'          => __( 'All post courses', 'sg' ),
		'view_item'          => __( 'View post courses on site', 'sg' ),
		'search_items'       => __( 'Search post courses', 'sg' ),
		'not_found'          => __( 'Post courses not found', 'sg' ),
		'not_found_in_trash' => __( 'Post courses not found in trash', 'sg' ),
		'menu_name'          => 'Courses'
	);
	$args   = array(
		'labels'        => $labels,
		'public'        => true,
		'show_ui'       => true,
		'has_archive'   => true,
		'menu_position' => 4,
		'taxonomies'    => array( 'course_category', 'course_type' ),
		'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields' )
	);
	register_post_type( 'courses', $args );
	flush_rewrite_rules( false );
}

/**
 * Add type post Courses banner
 */
add_action( 'init', 'courses_banner_register_post_type_init' );
function courses_banner_register_post_type_init() {
	$labels = array(
		'name'               => __( 'Courses banner', 'sg' ),
		'singular_name'      => __( 'Courses banner', 'sg' ),
		'add_new'            => __( 'Add new post Courses banner', 'sg' ),
		'add_new_item'       => __( 'Add new post Courses banner', 'sg' ),
		'edit_item'          => __( 'Edit post Courses banner', 'sg' ),
		'new_item'           => __( 'New post Courses banner', 'sg' ),
		'all_items'          => __( 'All post Courses banner', 'sg' ),
		'view_item'          => __( 'View post Courses banner on site', 'sg' ),
		'search_items'       => __( 'Search post Courses banner', 'sg' ),
		'not_found'          => __( 'Post Courses banner not found', 'sg' ),
		'not_found_in_trash' => __( 'Post Courses banner not found in trash', 'sg' ),
		'menu_name'          => 'Courses banner'
	);
	$args   = array(
		'labels'        => $labels,
		'public'        => true,
		'show_ui'       => true,
		'has_archive'   => true,
		'menu_position' => 4,
		'supports'      => array( 'title', 'editor', 'author', 'thumbnail' )
	);
	register_post_type( 'courses_banner', $args );
	flush_rewrite_rules( false );
}

/**
 * Add type post Small post
 */
add_action( 'init', 'small_post_register_post_type_init' );
function small_post_register_post_type_init() {
	$labels = array(
		'name'               => __( 'Small post', 'sg' ),
		'singular_name'      => __( 'Small post', 'sg' ),
		'add_new'            => __( 'Add new Small post', 'sg' ),
		'add_new_item'       => __( 'Add new Small post', 'sg' ),
		'edit_item'          => __( 'Edit Small post', 'sg' ),
		'new_item'           => __( 'New Small post', 'sg' ),
		'all_items'          => __( 'All Small post', 'sg' ),
		'view_item'          => __( 'View Small post on site', 'sg' ),
		'search_items'       => __( 'Search Small post', 'sg' ),
		'not_found'          => __( 'Small post not found', 'sg' ),
		'not_found_in_trash' => __( 'Small post not found in trash', 'sg' ),
		'menu_name'          => 'Small post'
	);
	$args   = array(
		'labels'        => $labels,
		'public'        => true,
		'show_ui'       => true,
		'has_archive'   => true,
		'menu_position' => 4,
		'supports'      => array( 'title', 'editor', 'author', 'thumbnail' )
	);
	register_post_type( 'small_post', $args );
	flush_rewrite_rules( false );
}

/**
 * Add type post Instructor
 */
add_action( 'init', 'instructor_register_post_type_init' );
function instructor_register_post_type_init() {
	$labels = array(
		'name'               => __( 'Instructor', 'sg' ),
		'singular_name'      => __( 'Instructor', 'sg' ),
		'add_new'            => __( 'Add new post Instructor', 'sg' ),
		'add_new_item'       => __( 'Add new post Instructor', 'sg' ),
		'edit_item'          => __( 'Edit post Instructor', 'sg' ),
		'new_item'           => __( 'New post Instructor', 'sg' ),
		'all_items'          => __( 'All post Instructor', 'sg' ),
		'view_item'          => __( 'View post Instructor on site', 'sg' ),
		'search_items'       => __( 'Search post Instructor', 'sg' ),
		'not_found'          => __( 'Post Instructor not found', 'sg' ),
		'not_found_in_trash' => __( 'Post Instructor not found in trash', 'sg' ),
		'menu_name'          => 'Instructor'
	);
	$args   = array(
		'labels'        => $labels,
		'public'        => true,
		'show_ui'       => true,
		'has_archive'   => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-welcome-learn-more',
		'supports'      => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields' )
	);
	register_post_type( 'instructor', $args );
	flush_rewrite_rules( false );
}

/**
 * Add type post Student say
 */
add_action( 'init', 'student_say_register_post_type_init' );
function student_say_register_post_type_init() {
	$labels = array(
		'name'               => __( 'Student say', 'sg' ),
		'singular_name'      => __( 'Student say', 'sg' ),
		'add_new'            => __( 'Add new post Student say', 'sg' ),
		'add_new_item'       => __( 'Add new post Student say', 'sg' ),
		'edit_item'          => __( 'Edit post Student say', 'sg' ),
		'new_item'           => __( 'New post Student say', 'sg' ),
		'all_items'          => __( 'All post Student say', 'sg' ),
		'view_item'          => __( 'View post Student say on site', 'sg' ),
		'search_items'       => __( 'Search post Student say', 'sg' ),
		'not_found'          => __( 'Post Student say not found', 'sg' ),
		'not_found_in_trash' => __( 'Post Student say not found in trash', 'sg' ),
		'menu_name'          => 'Student say'
	);
	$args   = array(
		'labels'        => $labels,
		'public'        => true,
		'show_ui'       => true,
		'has_archive'   => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-megaphone',
		'supports'      => array( 'title', 'editor', 'author', 'thumbnail' )
	);
	register_post_type( 'student_say', $args );
	flush_rewrite_rules( false );
}

/**
 * Register taxonomy for courses
 */
add_action('init', 'create_taxonomy');
function create_taxonomy() {
	register_taxonomy( 'course_type', array( 'courses' ), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'course_type'),
		'hierarchical'          => true,
        ) );
	register_taxonomy( 'course_category', array( 'courses' ), array(
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'course_category'),
		'hierarchical'          => true,
        ) );
}

/**
 * Additional search
 *
 * @param $query array
 *
 * @return array
 */
function advanced_search_query($query) {

	if($query->is_search) {
		if (isset($_GET['post_type']) && !empty($_GET['post_type'])) {
			$query->set( 'post_type', $_GET["post_type"] );
		}
		if (isset($_GET['course_type']) && !empty($_GET['course_type'])) {
			$query->set( 'course_type', $_GET["course_type"] );
		}
		if (isset($_GET['course_category']) && !empty($_GET['course_category'])) {
			$query->set( 'course_category', $_GET["course_category"] );
		}
		return $query;
	}
}
add_action('pre_get_posts', 'advanced_search_query', 1000);
