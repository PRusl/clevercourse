
<!DOCTYPE>
<html>
<head>
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--<body class="home page-template-default page page-id-3261 _masterslider _msp_version_3.1.3 tribe-js">-->
<div class="body-wrapper  float-menu" data-home="http://demo.goodlayers.com/clevercourse">
    <header class="gdlr-header-wrapper">
        <!-- top navigation -->
        <div class="top-navigation-wrapper" style="display: block;">
            <div class="top-navigation-container container">
                <?php dynamic_sidebar( 'top_left_sidebar' ); ?>
                <div class="top-navigation-right">
                    <?php dynamic_sidebar( 'top_right_sidebar' ); ?>
                </div>
            </div>
        </div>
<!--        <div class="clear"></div>-->
        <div class="gdlr-header-inner">
            <div class="gdlr-header-container container">
                <!-- logo -->
                <div class="gdlr-logo">

                    <?php the_custom_logo(); ?>
                </div>
                <div class="gdlr-navigation-wrapper">
                        <?php wp_nav_menu(
                            array('container' => 'nav',
                                'container_class' => 'gdlr-navigation sf-js-enabled sf-arrows',
                                'container_id' => 'gdlr-main-navigation',
                                'menu_class' => 'sf-menu gdlr-main-menu',
                                'menu_id' => 'menu-main-menu-1',
                                'items_wrap' => '<ul id="menu-main-menu-1" class="sf-menu gdlr-main-menu">
                                                    %3$s
                                                    <div class="gdlr-nav-search-form-button" id="gdlr-nav-search-form-button">
                                                    </div>
                                                    
                                                 </ul>'
                            ));
                        ?>
                </div>
            </div>
        </div>
    </header>
