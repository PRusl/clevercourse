<form class="navbar-form navbar-left" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="form-group">
        <input type="text" value="<?php echo get_search_query() ?>" name="s" id="s" />
      </div>
      <input type="submit" name="submit" class="btn btn-default" id="searchsubmit" value="" />
</form>