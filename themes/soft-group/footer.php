<footer class="footer-wrapper">
    <div class="footer-container container">
        <?php dynamic_sidebar( 'footer_sidebar' ); ?>
    </div>
    <div class="clear"></div>
    <div class="copyright-wrapper">
        <?php dynamic_sidebar( 'copyright_sidebar' ); ?>
    </div>
</footer>

</div><!-- body-wrapper -->
<?php wp_footer(); ?>
</body>
</html>