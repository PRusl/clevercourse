<?php if ( have_posts() ) : ?>

	<?php
	if ( ! is_front_page() ) {
		//Not a Main page
		?>

        <div class="gdlr-page-title-wrapper">
            <div class="gdlr-page-title-overlay"></div>
            <div class="gdlr-page-title-container container">
                <h1 class="gdlr-page-title"><?php
					add_filter( 'document_title_parts', function ( $parts ) {
						if ( isset( $parts['site'] ) ) {
							unset( $parts['site'] );
						}

						return $parts;
					} );
					echo esc_html( wp_get_document_title() );
					?></h1>
                <span class="gdlr-page-caption"></span>
            </div>
        </div>

		<?php
	} else {
		//The Main page
	}


	?>
	<?php if ( is_archive() ) { ?>
        <div class="with-sidebar-wrapper">
        <section id="content-section-1">
        <div class="section-container container">
        <div class="blog-item-wrapper">
        <div class="blog-item-holder">
        <div class="gdlr-isotope" data-type="blog" data-layout="fitRows">
        <div class="clear"></div>

	<?php } ?>

	<?php while ( have_posts() ) : the_post(); ?>


		<?php
		if ( 'instructor-list-style-1' == get_the_title() ) {
			?>
            <div class="content-wrapper">
                <div class="gdlr-content">

                    <!-- Above Sidebar Section-->

                    <!-- Sidebar With Content Section-->
                    <div class="with-sidebar-wrapper">
                        <section id="content-section-1">
                            <div class="section-container container">
                                <div class="instructor-item-wrapper" style="margin-bottom: 30px;">
                                    <div class="gdlr-lms-instructor-grid-wrapper">
                                        <div class="clear"></div>
										<?php $args       = array(
											'numberposts' => 10,
											'post_type'   => 'instructor',
											'order'       => 'ASC',
											'orderby'     => 'date'
										);
										$instructor_posts = get_posts( $args );
										?>

										<?php foreach ( $instructor_posts as $instructor_post ) { ?>
                                            <div class="gdlr-lms-instructor-grid gdlr-lms-col3">
                                                <div class="gdlr-lms-item">
                                                    <div class="gdlr-lms-instructor-content">
                                                        <div class="gdlr-lms-instructor-thumbnail">
															<?php echo get_the_post_thumbnail( $instructor_post->ID, 'thumbnail' ); ?>
                                                        </div>
                                                        <div class="gdlr-lms-instructor-title-wrapper">
                                                            <h3 class="gdlr-lms-instructor-title"><?php echo $instructor_post->post_title ?></h3>
                                                            <div class="gdlr-lms-instructor-position">
																<?php echo get_post_custom_values( 'position', $instructor_post->ID )[0]; ?>
                                                            </div>
                                                        </div>
                                                        <div class="gdlr-lms-author-description"><?php echo $instructor_post->post_content ?>
                                                        </div>
                                                        <a class="gdlr-lms-button cyan"
                                                           href="<?php echo get_permalink( $instructor_post->ID ); ?>">
                                                            View Profile</a>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>

										<?php } ?>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </section>
                    </div>

                    <!-- Below Sidebar Section-->


                </div><!-- gdlr-content -->
                <div class="clear"></div>
            </div>
			<?php
		} else {
			if ( is_category() ) {
				//Category page
			}
			elseif ( is_archive() ) { ?>
                <div class="four columns">
                    <div class="gdlr-item gdlr-blog-grid">
                        <div class="gdlr-ux gdlr-blog-grid-ux">

                            <article id="post"
                                     class="post post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row category-life-style tag-blog tag-life-style">
                                <div class="gdlr-standard-style">
                                    <div class="gdlr-blog-thumbnail">
                                        <a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail( 'archive' ); ?>
                                        </a>

                                    </div>

                                    <header class="post-header">
                                        <h3 class="gdlr-blog-title">
                                            <a href="<?php the_permalink(); ?>">

												<?php the_title(); ?>
                                            </a>
                                        </h3>

                                        <div class="gdlr-blog-info gdlr-info">
                                            <div class="blog-info blog-date">
                                                <span class="gdlr-head">Posted on</span>
												<?php echo get_the_date(); ?>
                                            </div>
                                            <div class="blog-info blog-tag">
                                                <span class="gdlr-head">Tags</span>
												<?php the_tags( '', '<span class="sep">,</span>', '' ); ?>
                                            </div>
                                            <div class="blog-info blog-comment">
                                                <span class="gdlr-head">Comments</span>
                                                <a href="<?php comments_link(); ?>">
													<?php comments_number( '0', '1', '%' ); ?>
                                                </a>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </header><!-- entry-header -->

                                    <div class="gdlr-blog-content">
										<?php the_excerpt(); ?>
                                        <div class="clear"></div>
                                        <a href="<?php the_permalink(); ?>"
                                           class="gdlr-button with-border excerpt-read-more">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </article><!-- #post -->
                        </div>
                    </div>
                </div>

				<?php
			}
			else {
				//Page
				?>
				<?php
				if ( ! is_front_page() ) {
					//Not a Main page
					?>
					<?php the_title('<h1>','</h1>'); ?>
                    <?php the_post_thumbnail( 'full' ); ?>

					<?php the_content(); ?>
                    <br>
                    <hr>
					<?php
				}
			}
			?>


		<?php } ?>

	<?php endwhile; ?>

	<?php if ( is_archive() ) { ?>
        </div>
        <div class="clear"></div>
        </div>
        </div>
        <div class="clear"></div>
        </div>
        </section>
        </div>

	<?php } ?>
<?php endif; ?>